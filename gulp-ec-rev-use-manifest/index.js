'use strict';
var _            = require('underscore');
var gutil        = require('gulp-util');
var PluginError  = gutil.PluginError;
var through      = require('through2');

var PLUGIN_NAME  = 'gulp-ec-rev-use-manifest';

var defaults = {
    revRegex: '-[0-9a-f]{8}-?', // one dash, eight numbers or letters a-f, and maybe another dash
    replaceReved: false, // Update any files that have already been revved
    debug: 0 // Debug level
};

function main(manifest, opts) {
    opts = _.defaults((opts || {}), defaults);

    var manifestData = [];

    for (var sourceName in manifest){
        var targetName = manifest[sourceName];
        var revved = false;

        if (opts.replaceReved) {
            var splitSource = sourceName.split(".");
            var post = splitSource.length ? "\." + splitSource.pop() : "";
            var pre = splitSource.join('\.');
            revved = new RegExp(pre + opts.revRegex + post, 'g');
        }

        manifestData.push({
            'source': sourceName,
            'target': targetName,
            'revved': revved,
            'unrevved': new RegExp(sourceName, 'g')
        });
    }
    if (opts.debug) console.log(PLUGIN_NAME+" - manifestData: ", manifestData);

    var stream = through.obj(function(file, enc, cb) {

        if (file.isNull()) {
            console.log(PLUGIN_NAME+" - Error: '"+file.relative+"' is null");
        }else if(file.isStream()) {
            console.log(PLUGIN_NAME+" - Error: '"+file.relative+"' is a stream, which isn't supported");
        }else{
            if (opts.debug) console.log(PLUGIN_NAME+" - Updating: '"+file.relative+"'");

            var contents = file.contents.toString();
            if (opts.debug > 2) console.log(contents);

            manifestData.forEach(function (data) {
                if (opts.debug > 1) {
                    var findingsDbgMsg = PLUGIN_NAME + " - > Found ";

                    var matches, numMatches, total, plural;

                    if (data.revved) {
                        matches = contents.match(data.revved);
                        numMatches = matches ? matches.length : 0;
                        total += numMatches;
                        plural = true;
                        findingsDbgMsg += numMatches + " revved and ";
                    }

                    matches = contents.match(data.source);
                    numMatches = matches ? matches.length : 0;
                    total = numMatches;
                    plural = plural || total != 1;
                    findingsDbgMsg += numMatches + " unrevved";

                    findingsDbgMsg += " instance" + ( plural ? 's' : '' ) + " of '" + data.source + "'";
                    console.log(findingsDbgMsg);
                }
                if (data.revved) contents = contents.replace(data.revved, data.target); // Update already revved
                contents = contents.replace(data.unrevved, data.target); // Update unrevved
            });

            file.contents = new Buffer(contents); // Write changes
        }

        this.push(file); // make sure the file goes through the next gulp plugin
        cb(); // tell the stream engine that we are done with this file
    });

    return stream; // returning the file stream
}

module.exports = main;
