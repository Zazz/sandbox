//
// CORE & UTILS
//

var gulp 		= require('gulp');
var exec		= require('child_process').exec;
var sys			= require('sys');
var util 		= require('gulp-util');
var notify 		= require('gulp-notify');	//In-stream notification
var notifier 	= require('node-notifier');	//Notification outside of stream

var plugins	= require("gulp-load-plugins")({
	lazy:false,
	pattern: ['gulp-*', 'gulp.*'],
	replaceString: /\bgulp[\-.]/
 });

//
// FILES
//

var size 		= require('gulp-size');
var concat 		= require('gulp-concat');
var rename 		= require('gulp-rename');
var cached 		= require('gulp-cached');
var remember 	= require('gulp-remember');
var header 		= require('gulp-header');
var footer	 	= require('gulp-footer');
var rev		 	= require('gulp-rev');	// https://github.com/sindresorhus/gulp-rev/blob/master/integration.md
										// https://github.com/sindresorhus/gulp-rev#works-with-gulp-rev
										// Do we want an on-the-fly lookup, or update all references (parse php/js, & compile handlebars)
var useManifest = require('./gulp-ec-rev-use-manifest/index.js');


//
//CSS
//

var minifycss 			= require('gulp-minify-css');
var autoprefixer 		= require('gulp-autoprefixer');
var combineMediaQueries	= require('gulp-combine-media-queries');

//
//JS
//

var jshint	= require('gulp-jshint');
var uglify	= require('gulp-uglify');

//
// PATHS
//

var basePaths = {
	build:	'build',
	source:	'source'
};
var sourcePath = {
	css:	basePaths.source + '/css',
	js:		basePaths.source + '/js'
};
var buildPath = {
	css:	basePaths.build + '/css',
	js:		basePaths.build + '/js'
};

//
// TASKS
//

/**
 * Print debug logs
 */
gulp.task('debugStart', function(){
	var key;

	console.log("Plugins:");
	for (key in plugins) console.log('\t'+key);

	console.log("Flags:");
	for (key in util.env) console.log('\t'+key);

});

/**
 * Process CSS
 */
gulp.task('css', function()
{
	var isDev = util.env.dev === true; //You can run a dev build by adding '--dev'
	console.log(isDev ? "CSS Development build" : "CSS Production build");

	gulp.src(sourcePath.css+'/*.css')
	//	.pipe(size({title:'Pre CSS', showFiles:true}))		// print all files
		.pipe(cached('css')) 								// only pass through changed files
		.pipe(size({title:'Cached CSS', showFiles:true}))	// print cached files
		.pipe(autoprefixer("last 20 version"))				// prefix CSS

		.pipe(remember('css')) 									// add back all files to the stream
		.pipe(concat("all.min.css"))							// concat all files
		.pipe( isDev ? util.noop() : combineMediaQueries() )	// combine media queries
		.pipe(minifycss())										// minify

		.pipe(rev())											// rev file name
		.pipe(gulp.dest(buildPath.css))							// write file
		.pipe(rev.manifest({merge:true}))						// add it to the manifest
		.pipe(gulp.dest(buildPath.css))							// write manifest

	//	.pipe(size({title:'Post CSS', showFiles:true}))
	//	.pipe(notify({message:"CSS Done", onLast:true}))
	;
});

/**
 * Process JavaScript
 */
gulp.task('js', function()
{
	gulp.src(sourcePath.js+'/*.js')
	//	.pipe(size({title:'Pre JS', showFiles:true}))

		.pipe(cached('js'))				// only pass through changed files
	//	.pipe(size({title:'Cached JS', showFiles:true}))
		.pipe(uglify())
		.pipe(jshint())					// do special things to the changed files...
		.pipe(header('(function () {'))	// e.g. jshinting ^^^
		.pipe(footer('})();'))			// and some kind of module wrapping

		.pipe(remember('js'))				// add back all files to the stream
		.pipe(concat('scripts.min.js'))		// do things that require all files
		.pipe(rev())						// rev file name
		.pipe(gulp.dest(buildPath.js))		// write file
		.pipe(rev.manifest({merge:true}))	// create manifest
		.pipe(gulp.dest(buildPath.js))		// write manifest

	//	.pipe(size({title:'Post JS', showFiles:true}))
	//	.pipe(notify({message:"JS Done", onLast:true}))
	;
});

gulp.task('manifest', function(){
	var jsManifest = require('./'+buildPath.js+'/rev-manifest.json');

	gulp.src('index.html')
		.pipe(size({title:'Revving', showFiles:true}))
		.pipe(useManifest(
			jsManifest,
			{
				replaceReved: true,
				debug: 2
			}
		))
		.pipe(rename('index-revved.html'))
		.pipe(size({title:'Revved', showFiles:true}))
		.pipe(gulp.dest('./'))
	;
});

gulp.task('reving', ['js'], function(){
	var jsManifest = require('./'+buildPath.js+'/rev-manifest.json');
	console.log(jsManifest);


	/*
	gulp.src('index.html')
		.pipe(size({title:'Revving', showFiles:true}))
		.pipe(revCollector({
			replaceReved: true,
			dirReplacements: jsManifest
		}))
		.pipe(rename('index-revved.html'))
		.pipe(size({title:'Revved', showFiles:true}))
		.pipe(gulp.dest('./'))
	;
	*/

});

/**
 * Just add a notification when files have been updated
 */
gulp.task('notifyUpdated', function(){
	notifier.notify({ 'title': 'Gulp Notification', 'message': 'Files Updated' });
});

/**
 * Exec is great for 'phpunit'
 */
gulp.task('list', function(){
	exec('ls', function(error, stdout){
		sys.puts(stdout);
	});
});

gulp.task('phpunit', function(){
	exec('phpunit', function(error, stdout){
		sys.puts(stdout);
	});
});

//
// TASK STACKS
//

/**
 *
 */
gulp.task('default', ['debugStart', 'css', 'js'], function(){
	notifier.notify({
		'title': 'Gulp Notification',
		'message': 'Finished default task'
	});
});

/**
 *
 */
gulp.task('watcher', function()
{
	gulp.watch(sourcePath.css+'/*.css', ['css', 'notifyUpdated']);
	gulp.watch(sourcePath.js+'/*.js', ['js', 'notifyUpdated']);
});